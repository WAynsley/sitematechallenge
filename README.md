# SitemateChallenge
# William Aynsley's Project

## Overview
This is a full-stack project comprised of a backend service built with .NET C# and a frontend application created using React. 


### Prerequisites
- Node.js v18.15.0
- npm v9.5.0

### Setup
# Clone repo
```bash
git git@gitlab.com:WAynsley/sitematechallenge.git
```


# Backend - .NET C# API
1. Navigate into the directory: `cd SitemateBackend`
2. To run the backend server run the command - `cd SitemateBackend && dotnet run`
3. Server should be running locally on `https://localhost:7152`


# Frontend - React Typescript
1. Open a serperate terminal whilst the server is running
2. Navigate into the directory: `cd sitemate-frontend`
3. Install dependencies: `npm install`
4. To run the frontend jest tests - run command `npm test` and hit the 'a' for all tests
5. To run the frontend application - run command `npm start`


## Future improvements
- Add validation 
- Add in a JWT service
- Add a caching service
- Add more to the pipeline, more automated tests, integration tests
- Deploy to a live environment
- Use Docker file 
