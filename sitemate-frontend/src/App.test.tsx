import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders William Aynsley Sitemate challenge link', () => {
  render(<App />);
  const linkElement = screen.getByText(/William Aynsley Sitemate challenge/i);
  expect(linkElement).toBeInTheDocument();
});
