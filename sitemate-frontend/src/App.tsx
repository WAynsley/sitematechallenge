import React from 'react';
import logo from './logo.svg';
import './App.css';
import SitemateClient from './components/SitemateClient';

function App() {
  return (
    <div className="App">
      <h1>William Aynsley Sitemate challenge</h1>
      <SitemateClient></SitemateClient>
    </div>
  );
}

export default App;
