import React, { useState } from "react";
import { fetchUtil } from "../utils/api";

const apiUrl = "https://localhost:7152/api/v1/sitemate";

const SitemateClient: React.FC = () => {
  const [name, setName] = useState("");
  const [age, setAge] = useState<number | null>(null);
  const [feedback, setFeedback] = useState("");

  const handleCreate = async () => {
    const data = await fetchUtil(apiUrl, "POST", { name, age });
    setFeedback(JSON.stringify(data));
  };

  const handleRead = async () => {
    const data = await fetchUtil(apiUrl);
    setFeedback(JSON.stringify(data));
  };

  const handleUpdate = async () => {
    const data = await fetchUtil(apiUrl, "PUT", { name, age });
    setFeedback(JSON.stringify(data));
  };

  const handleDelete = async () => {
    const data = await fetchUtil(`${apiUrl}/1`, "DELETE"); 
    setFeedback(JSON.stringify(data));
  };

  return (
    <div>
      <input
        placeholder="Name"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      <input
        type="number"
        placeholder="Age"
        value={age ?? ""}
        onChange={(e) => setAge(Number(e.target.value))}
      />

      <div>
        <button onClick={handleCreate}>Create</button>
        <button onClick={handleRead}>Read</button>
        <button onClick={handleUpdate}>Update</button>
        <button onClick={handleDelete}>Delete</button>
      </div>

      <div>
        <h3>Feedback:</h3>
        <p>{feedback}</p>
      </div>
    </div>
  );
};

export default SitemateClient;
