import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import SitemateClient from './SitemateClient';
import * as api from '../utils/api'; // Import the module where fetchUtil resides

// Mock the fetchUtil function
jest.mock('../utils/api');

describe('SitemateClient Component', () => {

  beforeEach(() => {
    (api.fetchUtil as jest.Mock).mockClear();
  });

  test('renders all buttons', () => {
    render(<SitemateClient />);
    expect(screen.getByText(/Create/i)).toBeInTheDocument();
    expect(screen.getByText(/Read/i)).toBeInTheDocument();
    expect(screen.getByText(/Update/i)).toBeInTheDocument();
    expect(screen.getByText(/Delete/i)).toBeInTheDocument();
  });

  test('handleCreate calls fetchUtil with correct parameters', async () => {
    render(<SitemateClient />);
    fireEvent.click(screen.getByText(/Create/i));
    expect(api.fetchUtil).toHaveBeenCalledWith("https://localhost:7152/api/v1/sitemate", "POST", { name: "John", age: 25 });
  });

  test('handleRead calls fetchUtil correctly', async () => {
    render(<SitemateClient />);
    fireEvent.click(screen.getByText(/Read/i));
    expect(api.fetchUtil).toHaveBeenCalledWith("https://localhost:7152/api/v1/sitemate");
  });

  test('handleUpdate calls fetchUtil with correct parameters', async () => {
    render(<SitemateClient />);
    fireEvent.click(screen.getByText(/Update/i));
    expect(api.fetchUtil).toHaveBeenCalledWith("https://localhost:7152/api/v1/sitemate", "PUT", { name: "Jane", age: 30 });
  });

  test('handleDelete calls fetchUtil correctly', async () => {
    render(<SitemateClient />);
    fireEvent.click(screen.getByText(/Delete/i));
    expect(api.fetchUtil).toHaveBeenCalledWith("https://localhost:7152/api/v1/sitemate/1", "DELETE");
  });

});
