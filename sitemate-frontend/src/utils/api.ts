const AUTH_HEADER = process.env.REACT_APP_AUTH_HEADER;

export const fetchUtil = async (
    endpoint: string,
    method: string = "GET",
    body?: any
  ) => {
    const headers: { [key: string]: string } = {
      "Content-Type": "application/json",
      "Authorization": "Basic V0F5bnNsZXk6UzNjVVJFUGFzc1dPUkQ="
    };
  
    try {
      const response = await fetch(endpoint, {
        method,
        headers,
        body: body ? JSON.stringify(body) : null,
      });
  
      if (!response.ok) {
        throw new Error(`Failed with status: ${response.status}`);
      }
  
      return await response.json();
    } catch (error) {
      console.error("Error during fetch:", error);
      throw error;
    }
  };