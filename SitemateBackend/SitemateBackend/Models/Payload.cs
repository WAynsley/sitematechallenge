﻿using System;
namespace SitemateBackend.Models
{
	public class Payload
	{
        public string Name { get; set; }
        public int Age { get; set; }
    }
}

