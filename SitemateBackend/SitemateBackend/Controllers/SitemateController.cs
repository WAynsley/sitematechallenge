﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SitemateBackend.Models;
using System;
using System.Collections.Generic;

namespace SitemateBackend.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class SitemateController : ControllerBase
    {
        private readonly ILogger<SitemateController> _logger;

        private static List<Payload> dataStore = new List<Payload>();

        public SitemateController(ILogger<SitemateController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public IActionResult Create([FromBody] Payload item)
        {
            try
            {
                dataStore.Add(item);
                _logger.LogInformation("Create operation with payload: {Payload}", item);
                return Ok(new { message = "Object added successfully." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while creating item.");
                return StatusCode(500, "Internal server error.");
            }
        }

        [HttpGet]
        public IActionResult Read()
        {
            try
            {
                return Ok(dataStore);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while reading items.");
                return StatusCode(500, "Internal server error.");
            }
        }

        [HttpPut]
        public IActionResult Update([FromBody] Payload item)
        {
            try
            {
                int index = dataStore.FindIndex(i => i.Name == item.Name);
                if (index != -1)
                {
                    dataStore[index] = item;
                }
                else
                {
                    return NotFound(new { message = "Item not found." });
                }

                _logger.LogInformation("Update operation with payload: {Payload}", item);
                return Ok(new { message = "Object updated successfully." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while updating item.");
                return StatusCode(500, "Internal server error.");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                if (id >= 0 && id < dataStore.Count)
                {
                    dataStore.RemoveAt(id);
                    _logger.LogInformation("Delete operation with id: {Id}", id);
                    return Ok(new { message = $"Id {id} logged for deletion." });
                }
                else
                {
                    return NotFound(new { message = "Item not found." });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while deleting item.");
                return StatusCode(500, "Internal server error.");
            }
        }
    }
}
