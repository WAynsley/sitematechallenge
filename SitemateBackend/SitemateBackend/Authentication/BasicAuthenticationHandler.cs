﻿using System;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;

namespace SitemateBackend.Authentication
{
    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly string _username;
        private readonly string _password;

        public BasicAuthenticationHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            IOptions<BasicAuthSettings> basicAuthSettings,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock)
            : base(options, logger, encoder, clock)
        {
            _username = basicAuthSettings.Value.Username;
            _password = basicAuthSettings.Value.Password;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey("Authorization"))
                return AuthenticateResult.Fail("Authorization header is missing.");

            try
            {
                var authHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                if (authHeader.Scheme.Equals("basic", StringComparison.OrdinalIgnoreCase))
                {
                    var credentialsBytes = Convert.FromBase64String(authHeader.Parameter);
                    var credentials = Encoding.UTF8.GetString(credentialsBytes).Split(":");
                    if (credentials.Length != 2)
                        return AuthenticateResult.Fail("Invalid Authorization header format.");

                    var username = credentials[0];
                    var password = credentials[1];
                    if (username == _username && password == _password)
                    {
                        var claims = new[] { new Claim(ClaimTypes.Name, username) };
                        var identity = new ClaimsIdentity(claims, Scheme.Name);
                        var principal = new ClaimsPrincipal(identity);
                        var ticket = new AuthenticationTicket(principal, Scheme.Name);

                        return AuthenticateResult.Success(ticket);
                    }
                }
            }
            catch
            {
                return AuthenticateResult.Fail("Invalid Authorization header format.");
            }

            return AuthenticateResult.Fail("Invalid username or password.");
        }
    }
}

