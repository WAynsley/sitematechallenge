﻿using System;
namespace SitemateBackend.Authentication
{
    public class BasicAuthSettings
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}

